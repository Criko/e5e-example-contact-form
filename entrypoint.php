<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';
include_once("config.php");

/**
 * @param $event
 * @param $context
 * @return array
 */
function main($event, $context)
{
    $data = $event["data"];
    //Check request parameters
    $result = checkRequestParameters($data);
    if (!$result["error"]) {
        //Try to Send mail
        $mailSend = sendMail($data);
        if ($mailSend) {
            $response = [
                'status' => 201,
                'message' => "E-mail sent successfully",
            ];
        } else {
            $response = [
                'status' => 400,
                'message' => "Error while sending email",
            ];
        }
    } else {
        $response = [
            'status' => 400,
            'message' => $result["message"],
        ];
    }

    return $response;
}

/**
 * Try to send mail
 * @param $data
 * @return bool
 */
function sendMail($data)
{
    $mail = new PHPMailer(true);
    try {
        $msg ="New contact request from<br/>";
        $msg.="Firstname: ".$data["firstname"]."<br/>";
        $msg.="Lastname: ".$data["firstname"]."<br/>";
        $msg.="E-Mail: ".$data["email"]."<br/>";
        $msg.="Message:<br/>".nl2br($data["content"])."<br/>";
        //Server settings
        $mail->SMTPDebug = 0;
        // Set mailer to use SMTP
        $mail->isSMTP();
        // Specify main and backup SMTP servers
        $mail->Host = EMAIL_HOST;
        // Enable SMTP authentication
        $mail->SMTPAuth = true;
        // SMTP username
        $mail->Username = EMAIL_USERNAME;
        // SMTP password
        $mail->Password = EMAIL_PASSWORD;
        $mail->SMTPSecure = 'STARTTLS';
        // TCP port to connect to
        $mail->Port = EMAIL_PORT;
        //Recipients
        $mail->setFrom(EMAIL_FROM);
        $mail->addAddress(EMAIL_RECIPIENT);     // Add a recipient
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = EMAIL_SUBJECT;
        $mail->Body = $msg;
        $mail->send();
        return true;

    } catch (Exception $e) {
        return false;
    }
}

/**
 * Check request parameters
 * @param $data
 * @return array
 */
function checkRequestParameters($data)
{
    $errorMessage = "";
    $errorFields = "";
    //Define error messesages
    $errorMessages = [
        "requiredFields" => "Required fields are missing: ",
        "email" => "The email is not valid"
    ];
    //Define required fields
    $requiredFields = ["email", "firstname", "lastname", "content"];
    $error = false;
    //Check fields are set
    foreach ($requiredFields as $field) {
        if (!isset($data[$field])) {
            $error = true;
            $errorFields .= $field . ", ";
        } else {
            if ($data[$field] == "") {
                $error = true;
                $errorFields .= $field . ", ";
            }
        }
    }
    if (!$error) {
        //Check if email is correct
        if (!filter_var($data["email"], FILTER_VALIDATE_EMAIL)) {
            $errorMessage = $errorMessages["email"];
            $error = true;
        }
    } else {
        $errorMessage = $errorMessages["requiredFields"] . " " . substr($errorFields, 0, -2);
    }
    return ["error" => $error, "message" => $errorMessage];
}
