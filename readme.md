# e5e Contact-Form example

### To prepare the project for e5e the following steps are necessary
* Clone this example repo from git
* Switch to project directory (cd e5e-example-contact-form)
* Install composer (composer install) 
* Now the project is ready for e5e
* Create zip from project
* Loggin to e5e 
* Create new Application or chose an existing
* Create new Function
    * Chose Storage-Backend Zip
    * Chose Runtime (PHP>=7.2)
    * Set Entry point to entrypoint.php::main
    * Add your Environment variables (The required variables are described below)
    * Chose from performance and quota
    
### Add e5e function to frontier
* Create or chose an existing API
* Switch to your selected API 
* Create or chose an existing Endpoint
* Create new action
   * Endpoint is preselected a change is not necessary
   * Chose Request method (POST)
   * Chose Type E5E-Function
        * Select your created E5E-Function for example (Example-Contact-Form-Function)
    
    
### Following Environment variables are needed

* EMAIL_HOST
* EMAIL_USERNAME
* EMAIL_PASSWORD
* EMAIL_FROM
* EMAIL_PORT
* EMAIL_RECIPIENT
* EMAIL_SUBJECT

### Following fields are requiered
 
* firstname
* lastname
* email
* content